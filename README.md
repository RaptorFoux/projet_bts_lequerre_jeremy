# projet_bts_LEQUERRE_JEREMY

## Prérequis avant l'installation
#### Etape 1 : cloner le projet avec la commande
```
git clone https://gitlab.com/RaptorFoux/projet_bts_lequerre_jeremy.git
```

#### Etape 2 : Vérifier que psql est installé sur votre ordinateur et que vous avez accès aux lignes de commande psql.

#### Etape 3 : Créer la base de données avec la commande
```
createdb -U postgres poeleton
```

## Installation et lancement du backend
Ouvrer un nouveau terminal dans votre IDE puis faite la commande
```
cd server
```
puis pour installer les dépendances
```
yarn install
```
créer un fichier .env à la racine du dossier server avec les même données que le fichier .env.example, modifier la variable `PG_PASSWORD` si nécessaire, puis pour créer les tables dans la base de données
```
node ace migration:run
```
puis pour peupler la base de données
```
node ace db:seed
```
et enfin pour lancer le backend
```
yarn dev
```

## Installation et lancement du frontend
Ouvrer un nouveau terminal dans votre IDE puis faite la commande
```
cd client
```
puis pour installer les dépendances
```
yarn install
```
et enfin pour lancer le frontend
```
yarn dev
```

## Après installation
Après avoir suivi les deux points précédents vous pouvez maintenant tester le projet