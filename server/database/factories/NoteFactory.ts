import Note from 'App/Models/Note'
import Factory from '@ioc:Adonis/Lucid/Factory'
import OpinionFactory from './OpinionFactory'
import UserFactory from './UserFactory'

export default Factory.define(Note, async ({ faker }) => {
  const user = await UserFactory.create()
  return {
    userId: user.id,
    note: faker.number.int({ min: 1, max: 5 }),
  }
})
  .relation('opinion', () => OpinionFactory)
  .build()
