import Opinion from 'App/Models/Opinion'
import Factory from '@ioc:Adonis/Lucid/Factory'

export default Factory.define(Opinion, ({ faker }) => {
    return {
        opinion: faker.lorem.lines(),
    }
}).build()
