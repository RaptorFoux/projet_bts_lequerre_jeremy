import Recipe from 'App/Models/Recipe'
import Factory from '@ioc:Adonis/Lucid/Factory'
import NoteFactory from './NoteFactory'

export default Factory.define(Recipe, ({ faker }) => {
  return {
    title: faker.lorem.words({ min: 3, max: 12}),
    content: faker.lorem.sentences({min: 5, max: 100}),
  }
})
  .relation('notes', () => NoteFactory)
  .build()
