import User from 'App/Models/User'
import Factory from '@ioc:Adonis/Lucid/Factory'
import RecipeFactory from './RecipeFactory'
import NoteFactory from './NoteFactory'

export default Factory.define(User, ({ faker }) => {
  return {
    username: faker.internet.userName(),
    password: faker.internet.password(),
  }
})
  .relation('recipes', () => RecipeFactory)
  .relation('notes', () => NoteFactory)
  .build()
