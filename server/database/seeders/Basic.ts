import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import UserFactory from 'Database/factories/UserFactory'

export default class extends BaseSeeder {
    public environement = ['develeoppement', 'testing']
    public async run() {
        await UserFactory.with('recipes', 5, (recipe) =>
            recipe.with('notes', 3, (note) => note.with('opinion', 1))
        ).createMany(10)
    }
}
