import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import Recipe from 'App/Models/Recipe'

export default class RecipesController {
    public async index() {
        const result = await Database.rawQuery(
            `SELECT recipes.*,
                users.username,
                AVG(notes.note) as average_note,
                COUNT(notes.note) as note_count,
                COUNT(opinions.opinion) as opinion_count
            FROM recipes
            LEFT JOIN notes ON notes.recipe_id = recipes.id
            LEFT JOIN opinions ON opinions.note_id = notes.id
            LEFT JOIN users ON users.id = recipes.user_id
            GROUP BY recipes.id, users.username`
        )
        return result.rows
    },

    public async show({ params }: HttpContextContract) {
        try {
            const result = await Database.rawQuery(
                `SELECT recipes.*,
                    users.username,
                    AVG(notes.note) as average_note,
                    JSON_AGG(notes.*) as notes,
                    JSON_AGG(opinions.*) as opinions
                FROM recipes
                LEFT JOIN notes ON notes.recipe_id = recipes.id
                LEFT JOIN opinions ON opinions.note_id = notes.id
                LEFT JOIN users ON users.id = recipes.user_id
                WHERE recipes.id = ${params.id}
                GROUP BY recipes.id, users.username`
            )
            return result.rows[0]
        } catch (error) {
            console.log(error)
        }
    },

    public async update({ request, params, auth }: HttpContextContract) {
        const { title, content } = request.all()
        const recipe = await Recipe.find(params.id)
        // await auth.authenticate()
        if (recipe) {
            recipe.title = title
            recipe.content = content
            if (await recipe.save()) {
                return recipe
            }
        }
    },

    public async store({ request, auth }: HttpContextContract) {
        const { title, content } = request.all()
        // await auth.authenticate()
        const recipe = new Recipe()
        recipe.title = title
        recipe.content = content
        await recipe.save()
        return recipe
    },

    public async destroy({ auth, params }: HttpContextContract) {
        // await auth.authenticate()
        await Recipe.query().where('id', params.id).delete()
    },

    public async dashboard({ params, auth }: HttpContextContract) {
        // await auth.authenticate()
        return await Recipe.query().where('user_id', params.id)
    }
}
