import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
export default class AuthController {
    public async logout({ auth }: HttpContextContract) {
        await auth.logout()
    },

    public async login({ request, response, auth }: HttpContextContract) {
        const { username, password } = request.all()

        try {
            const token = await auth.attempt(username, password, {
                expiresIn: '3 days',
            })
            return token.toJSON()
        } catch (_error) {
            return response.json({ message: "Nom d'utilisateur ou mot de passe incorrect" })
        }
    },

    public async register({ request, response, auth }: HttpContextContract) {
        const { username, password } = request.all()

        const user = new User()
        user.username = username
        user.password = password

        try {
            await user.save()

            const token = await auth.attempt(username, password, {
                expiresIn: '3 days',
            })
            return token.toJSON()
        } catch (_error) {
            return response.json({ message: "Le nom d'utilisateur existe déjà" })
        }
    },
}
