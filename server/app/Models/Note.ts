import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, HasOne, belongsTo, column, hasOne } from '@ioc:Adonis/Lucid/Orm'
import Opinion from './Opinion'
import Recipe from './Recipe'
import User from './User'

export default class Note extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public userId: number

  @column()
  public recipeId: number

  @column()
  public note: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasOne(() => Opinion, {})
  public opinion: HasOne<typeof Opinion>

  @belongsTo(() => Recipe, {})
  public recipe: BelongsTo<typeof Recipe>

  @belongsTo(() => User, {})
  public user: BelongsTo<typeof User>
}
