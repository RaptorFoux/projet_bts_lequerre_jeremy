import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Note from './Note'

export default class Opinion extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public noteId: number

  @column()
  public opinion: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Note, {})
  public note: BelongsTo<typeof Note>
}
