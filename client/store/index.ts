import { defineStore } from "pinia";

export const useUserStore = defineStore('store', () => {
  const user = ref({})
  const isLogedIn = ref(false)

  function setUser(data: Object) {
    user.value = data
  }

  function setLogedIn(value: boolean) {
    isLogedIn.value = value
  }

  return { user, isLogedIn, setUser, setLogedIn }
})