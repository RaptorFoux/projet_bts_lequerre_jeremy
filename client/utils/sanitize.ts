export function sanitizeInput(input: string): string {
  if (typeof input !== 'string') {
    return '';
  }

  // Éviter les attaques XSS et les injections SQL
  const sanitizedInput = input
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&#x27;')
    .replace(/\//g, '&#x2F;');

  return sanitizedInput;
}