// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [ "@pinia/nuxt" ],
  css: [ "@/assets/style.scss" ],
  app: {
    head: {
      title: "Poeleton",
      link: [
        {
			rel: "preconnect",
			href: "https://fonts.gstatic.com"
        },
		{
			rel: "stylesheet",
			href: "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
		},
		{
			rel: "icon",
			type: "image/png",
			href: "/logo.png"
		}
      ]
    }
  }
})
